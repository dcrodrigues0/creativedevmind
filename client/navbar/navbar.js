window.onscroll = function() {myFunction()};

function myFunction() {
  if (document.body.scrollTop > 10 || document.documentElement.scrollTop > 10) {
    var nav = $('#my-nav');

    nav.removeClass("bg-light");
    nav.addClass("bg-scrolled");
    nav.addClass("navScrolled");

  } else if(document.body.scrollTop < 10 || document.documentElement.scrollTop < 10) {
    var nav = $('#my-nav');
    
    nav.removeClass("bg-scrolled");
    nav.addClass("bg-light");
    nav.removeClass("navScrolled");
  }
}

